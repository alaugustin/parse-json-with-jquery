function setResultsHeight() {
	var maxHeight = -1;
	$('#results li').each(function() {
		maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
	});

	$('#results li').each(function() {
		$(this).height(maxHeight);
	});
};